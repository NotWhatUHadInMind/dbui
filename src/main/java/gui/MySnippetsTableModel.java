package gui;

import com.google.common.collect.Lists;
import entities.Snippet;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.*;

public class MySnippetsTableModel implements TableModel {
    private List<Snippet> model;
    private List<String> columns;
    private List<String> viewColumns;

    private Map<Integer, Integer> snippetOrderById;

    private List<TableModelListener> listeners = new ArrayList<>();

    public MySnippetsTableModel(List<Snippet> model, List<String> columns) {
        this.model = model;
        this.columns = columns;
        this.viewColumns = Lists.newArrayList(columns);

        List<Integer> snippetsIds = new ArrayList<>();
        for (Snippet snippet : model) {
            snippetsIds.add(snippet.getId());
        }
        Collections.sort(snippetsIds);

        snippetOrderById = new HashMap<>();
        for (int i = 1; i <= snippetsIds.size(); i++) {
            snippetOrderById.put(snippetsIds.get(i - 1), i);
        }
    }

    @Override
    public int getRowCount() {
        return model.size();
    }

    @Override
    public int getColumnCount() {
        return viewColumns.size();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return viewColumns.get(columnIndex);
    }

    public Snippet getSnippetAtRow(int row) {
        return model.get(row);
    }

    public int getModelColumnIndex(String columnName) {
        return columns.indexOf(columnName);
    }

    public boolean columnIsGreater(String columnName, String withCompareColumn) {
        int index = columns.indexOf(withCompareColumn);
        return columns.indexOf(columnName) > index;
    }

    public void changeModel(List<Snippet> model) {
        this.model = model;

        List<Integer> snippetsIds = new ArrayList<>();
        for (Snippet snippet : model) {
            snippetsIds.add(snippet.getId());
        }
        Collections.sort(snippetsIds);

        snippetOrderById = new HashMap<>();
        for (int i = 1; i <= snippetsIds.size(); i++) {
            snippetOrderById.put(snippetsIds.get(i - 1), i);
        }
    }

    public List<Snippet> getModel() {
        return Collections.unmodifiableList(model);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return Integer.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return String.class;
            case 5:
                return String.class;
            case 6:
                return String.class;
            case 7:
                return String.class;
            case 8:
                return Boolean.class;
        }
        return Object.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 8;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return model.get(rowIndex).getQuery().getBody();
            case 1:
                return snippetOrderById.get(model.get(rowIndex).getId());
            case 2:
                return model.get(rowIndex).getSnippet();
            case 3:
                return model.get(rowIndex).getPageUrl();
            case 4:
                return model.get(rowIndex).getCachePageUrl();
            case 5:
                return model.get(rowIndex).getPageBody();
            case 6:
                return model.get(rowIndex).getDescription();
            case 7:
                return model.get(rowIndex).getSnippetBody();
            case 8:
                return model.get(rowIndex).isChecked();
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                model.get(rowIndex).getQuery().setBody((String)aValue);
                break;
            case 1:
                break;
            case 2:
                model.get(rowIndex).setSnippet((String) aValue);
                break;
            case 3:
                model.get(rowIndex).setPageUrl((String)aValue);
                break;
            case 4:
                model.get(rowIndex).setCachePageUrl((String)aValue);
                break;
            case 5:
                model.get(rowIndex).setPageBody((String)aValue);
                break;
            case 6:
                model.get(rowIndex).setDescription((String)aValue);
                break;
            case 7:
                model.get(rowIndex).setSnippetBody((String)aValue);
                break;
            case 8:
                model.get(rowIndex).setChecked((Boolean)aValue);
                break;
        }
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }
}