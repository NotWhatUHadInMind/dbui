package gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.cfg.Configuration;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by Vladislav on 20.10.2015.
 */
public class DatabaseSettingsForm {
    public static final String INVALID_SETTING_MESSAGE = "Невалидные настройки";
    public static final String ERROR_DIALOG_TITLE = "Ошибка";

    private static final Logger LOG = LogManager.getLogger(DatabaseSettingsForm.class);
    private static final String DATABASE_SETTINGS_FORM_TITLE = "Настройки подключения к базе данных";

    private JTextField urlField;
    private JButton acceptButton;
    private JTextField userField;
    private JTextField passwordField;

    public JPanel mainPanel;
    public JFrame frame;

    public Configuration hibernateConfig;

    public DatabaseSettingsForm(Configuration hibernateConfig) {
        this.hibernateConfig = hibernateConfig;
        resetToPreviousSettings();

        frame = new JFrame(DATABASE_SETTINGS_FORM_TITLE);
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);

        acceptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String url = urlField.getText();
                String user = userField.getText();
                String password = passwordField.getText();

                try {
                    Configuration notCheckedConfig = new Configuration().
                            configure(System.getProperty(MainForm.HIBERNATE_CONFIG_PROPERTY)).
                            setProperty("hibernate.connection.url", url).
                            setProperty("hibernate.connection.username", user).
                            setProperty("hibernate.connection.password", password);
                    notCheckedConfig.buildSessionFactory();

                    DatabaseSettingsForm.this.hibernateConfig = notCheckedConfig;

                } catch (Exception ex) {
                    LOG.error("Invalid hibernate settings", ex);
                    JOptionPane.showMessageDialog(mainPanel, INVALID_SETTING_MESSAGE, ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
                    resetToPreviousSettings();
                }
            }
        });
    }

    private void resetToPreviousSettings() {
        if (hibernateConfig != null) {
            urlField.setText(hibernateConfig.getProperty("hibernate.connection.url"));
            userField.setText(hibernateConfig.getProperty("hibernate.connection.username"));
            passwordField.setText(hibernateConfig.getProperty("hibernate.connection.password"));
        }
    }
}
