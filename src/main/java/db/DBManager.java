package db;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import entities.Query;
import entities.Snippet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by vladislav on 20/10/15.
 */
public class DBManager {
    private static final Logger LOG = LogManager.getLogger(DBManager.class);
    private static final DBManager instance = new DBManager();
    private Configuration configuration;
    private Session currentSession;

    private DBManager() {
    }

    public static DBManager getInstance() {
        return instance;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    @SuppressWarnings("unchecked")
    public List<Snippet> getAllSnippetsBySnippetPart(final String snippetPart) throws DBException {
        try {
            commitAndReleaseCurrentSession();

            SessionFactory sessionFactory = buildSessionFactory();
            Session session = sessionFactory.openSession();
            session.getTransaction().begin();

            List<Snippet> snippets = session.createCriteria(Snippet.class).list();
            List<Snippet> filteredSnippets = new ArrayList<>(Collections2.filter(snippets, new Predicate<Snippet>() {
                @Override
                public boolean apply(Snippet snippet) {
                    return snippet.getSnippet().contains(snippetPart)
                            || (snippet.getDescription() != null && snippet.getDescription().contains(snippetPart));
                }
            }));

            currentSession = session;

            return filteredSnippets;
        } catch (HibernateException e) {
            LOG.error("Error during get snippets process", e);
            throw new DBException("Error during db process", e);
        }
    }

    private SessionFactory buildSessionFactory() {
        return configuration.buildSessionFactory();
    }

    public void commitAndReleaseCurrentSession() {
        if (currentSession != null) {
            currentSession.flush();
            currentSession.getTransaction().commit();
            currentSession.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<String> getAllQueries() throws DBException {
        try {
            SessionFactory sessionFactory = buildSessionFactory();
            Session session = sessionFactory.openSession();
            session.getTransaction().begin();

            List<String> queries = session.createCriteria(Query.class).
                    setProjection(Projections.projectionList().add(Projections.property("body"))).list();

            session.close();
            return queries;
        } catch (HibernateException e) {
            LOG.error("Error during get all queries process", e);
            throw new DBException("Error during db process", e);
        }
    }

}
