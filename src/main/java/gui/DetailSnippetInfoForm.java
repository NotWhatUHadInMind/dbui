package gui;

import entities.Snippet;

import javax.swing.*;

/**
 * Created by Vladislav on 01.11.2015.
 */
public class DetailSnippetInfoForm {
    private JPanel mainPanel;
    private JTextArea idValueArea;
    private JTextArea queryValueArea;
    private JTextArea descriptionValueArea;
    private JTextArea snippetValueArea;
    private JTextArea snippetWithRValueArea;
    private JTextArea pageTextValueArea;

    public JFrame frame;

    public DetailSnippetInfoForm(Snippet snippet) {
        idValueArea.setText(String.valueOf(snippet.getId()));
        queryValueArea.setText(snippet.getQuery().getBody());
        descriptionValueArea.setText(snippet.getDescription());
        snippetValueArea.setText(snippet.getSnippet());
        snippetWithRValueArea.setText(snippet.getSnippetBody());
        pageTextValueArea.setText(snippet.getPageBody());

        frame = new JFrame("Информация о сниппете");
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
}
