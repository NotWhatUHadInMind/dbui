package gui;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import db.DBManager;
import db.Exporter;
import entities.Snippet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.cfg.Configuration;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

/**
 * Created by Vladislav on 19.10.2015.
 */
public class MainForm {
    public static final String HIBERNATE_CONFIG_PROPERTY = "hibernateConfig";

    private static final Logger LOG = LogManager.getLogger(MainForm.class);

    private static final String MAIN_FORM_TITLE = "Просмотр базы данных";
    private static final String DATABASE_CONNECTION_ERROR = "Ошибка подключения к базе данных";
    private static final String EXPORT_ERROR = "Ошибка экспорта данных";

    private JPanel mainPanel;
    private JTable table;
    private JCheckBox queryNameCheckBox;
    private JCheckBox snippetCheckBox;
    private JCheckBox orderNumberCheckBox;
    private JCheckBox urlCheckBox;
    private JCheckBox cacheUrlCheckBox;
    private JCheckBox pageCheckBox;
    private JCheckBox descriptionCheckBox;
    private JCheckBox snippetBodyCheckBox;
    private JTextField searchField;
    private JButton findButton;
    private JButton settingsButton;
    private JCheckBox checkedFieldCheckBox;
    private JLabel foundLabel;
    private JComboBox<String> queriesBox;
    private JButton exportButton;

    private static JFrame mainFrame;
    private DatabaseSettingsForm databaseSettingsForm;

    private final List<JCheckBox> checkBoxes = new ArrayList<JCheckBox>() {{
        add(queryNameCheckBox);
        add(orderNumberCheckBox);
        add(snippetCheckBox);
        add(urlCheckBox);
        add(cacheUrlCheckBox);
        add(pageCheckBox);
        add(descriptionCheckBox);
        add(snippetBodyCheckBox);
        add(checkedFieldCheckBox);
    }};

    private MySnippetsTableModel mySnippetsTableModel;

    public static void main(String[] args) {
        System.setProperty(HIBERNATE_CONFIG_PROPERTY, "/hibernate.cfg.xml");
        System.setProperty("file.encoding", "UTF-8");

        mainFrame = new JFrame(MAIN_FORM_TITLE);
        mainFrame.setContentPane(new MainForm().mainPanel);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);

        mainFrame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                DBManager.getInstance().commitAndReleaseCurrentSession();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
    }

    public MainForm() {
        final List<String> columns = new ArrayList<>();
        for (JCheckBox checkBox : checkBoxes) {
            columns.add(checkBox.getText());
        }

        mySnippetsTableModel = new MySnippetsTableModel(new ArrayList<Snippet>(), columns);
        table.setModel(mySnippetsTableModel);
        table.getTableHeader().setEnabled(false);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable table =(JTable) e.getSource();

                    int selectedRow = table.getSelectedRow();
                    int modelRowIndex = table.getRowSorter().convertRowIndexToModel(selectedRow);

                    final DetailSnippetInfoForm snippetInfoForm =
                            new DetailSnippetInfoForm(mySnippetsTableModel.getSnippetAtRow(modelRowIndex));
                    snippetInfoForm.frame.setVisible(true);
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            snippetInfoForm.frame.toFront();
                        }
                    });
                }
            }
        });

        findButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    List<Snippet> allSnippetsByPart = DBManager.getInstance().
                            getAllSnippetsBySnippetPart(searchField.getText());

                    if (queriesBox.getSelectedItem() != null && !((String) queriesBox.getSelectedItem()).isEmpty()) {
                        allSnippetsByPart = new ArrayList<>(
                                Collections2.filter(allSnippetsByPart, new Predicate<Snippet>() {
                                    @Override
                                    public boolean apply(Snippet snippet) {
                                        return snippet.getQuery().getBody().equals(queriesBox.getSelectedItem());
                                    }
                                }));
                    }

                    mySnippetsTableModel.changeModel(allSnippetsByPart);
                    foundLabel.setText(foundLabel.getToolTipText() + allSnippetsByPart.size());

                    table.getTableHeader().setEnabled(!allSnippetsByPart.isEmpty());
                    table.getRowSorter().modelStructureChanged();
                    table.updateUI();
                } catch (Throwable ex) {
                    LOG.error("Error in dbManager", ex);
                    JOptionPane.showMessageDialog(mainPanel, DATABASE_CONNECTION_ERROR,
                            DatabaseSettingsForm.ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        ActionListener checkBoxListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBox checkBox = (JCheckBox) e.getSource();
                String columnName = checkBox.getText();
                if (checkBox.isSelected()) {
                    int modelIndex = mySnippetsTableModel.getModelColumnIndex(columnName);
                    table.addColumn(new TableColumn(modelIndex));

                    TableColumnModel model = table.getColumnModel();
                    int invalidIndex = model.getColumnIndex(columnName);

                    int swapColumnIndex = invalidIndex;
                    Enumeration<TableColumn> columnEnumeration = model.getColumns();
                    while (columnEnumeration.hasMoreElements()) {
                        TableColumn tableColumn = columnEnumeration.nextElement();
                        String header = (String) tableColumn.getHeaderValue();

                        if (!mySnippetsTableModel.columnIsGreater(columnName, header)) {
                            swapColumnIndex = model.getColumnIndex(header);
                            break;
                        }
                    }

                    model.moveColumn(invalidIndex, swapColumnIndex);
                } else {
                    table.removeColumn(table.getColumn(columnName));
                }
            }
        };

        queryNameCheckBox.addActionListener(checkBoxListener);
        snippetCheckBox.addActionListener(checkBoxListener);
        orderNumberCheckBox.addActionListener(checkBoxListener);
        urlCheckBox.addActionListener(checkBoxListener);
        cacheUrlCheckBox.addActionListener(checkBoxListener);
        pageCheckBox.addActionListener(checkBoxListener);
        descriptionCheckBox.addActionListener(checkBoxListener);
        snippetBodyCheckBox.addActionListener(checkBoxListener);
        checkedFieldCheckBox.addActionListener(checkBoxListener);

        table.removeColumn(table.getColumn(pageCheckBox.getText()));

        Configuration config = new Configuration().configure(System.getProperty(MainForm.HIBERNATE_CONFIG_PROPERTY));
        try {
            config.buildSessionFactory();
        } catch (Throwable e) {
            LOG.error("Invalid hibernate settings", e);
            JOptionPane.showMessageDialog(mainPanel, DatabaseSettingsForm.INVALID_SETTING_MESSAGE,
                    DatabaseSettingsForm.ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
        }

        DBManager.getInstance().setConfiguration(config);
        databaseSettingsForm = new DatabaseSettingsForm(config);
        databaseSettingsForm.frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                DBManager.getInstance().setConfiguration(databaseSettingsForm.hibernateConfig);
                mainFrame.setVisible(true);
                mainFrame.setEnabled(true);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.setEnabled(false);
                databaseSettingsForm.frame.setVisible(true);
            }
        });


        queriesBox.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                try {
                    List<String> queries = DBManager.getInstance().getAllQueries();
                    queries.add(0, null);
                    Set<String> gotQueries = new LinkedHashSet<>(queries);

                    JComboBox<String> comboBox = (JComboBox<String>) e.getSource();
                    Set<String> currentQueries = new HashSet<>();
                    for (int i = 0; i < comboBox.getModel().getSize(); i++) {
                        currentQueries.add(comboBox.getModel().getElementAt(i));
                    }

                    if (!currentQueries.equals(gotQueries)) {
                        comboBox.setModel(new DefaultComboBoxModel<>(gotQueries.toArray(new String[gotQueries.size()])));
                    }
                } catch (Throwable ex) {
                    LOG.error("Error in dbManager", ex);
                    JOptionPane.showMessageDialog(mainPanel, DATABASE_CONNECTION_ERROR,
                            DatabaseSettingsForm.ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
                }
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });

        exportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setDialogTitle("Выбор пути сохранения файла");

                try {
                    if (fileChooser.showSaveDialog(mainPanel) == JFileChooser.APPROVE_OPTION) {
                        Exporter.getInstance().exportToXML(fileChooser.getSelectedFile().getPath(), columns,
                                mySnippetsTableModel.getModel());
                    }
                } catch (Exception ex) {
                    LOG.error("Error during export process", ex);
                    JOptionPane.showMessageDialog(mainPanel, EXPORT_ERROR,
                            DatabaseSettingsForm.ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
}
