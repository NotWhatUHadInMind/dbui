package db;

import entities.Snippet;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

/**
 * Created by Vladislav on 05.11.2015.
 */
public class Exporter {
    private static final String FILE_NAME_PATTERN = "Snippets";
    private static final int MAX_CELL_SIZE = 32767;
    private static final int MAX_ROWS_COUNT = 65536;

    private static final Exporter instance = new Exporter();

    private Exporter(){
    }

    public static Exporter getInstance() {
        return instance;
    }

    public void exportToXML(String path, List<String> headers, List<Snippet> snippets) throws Exception {
        String filePrefix = "";
        int fileNumber = 0;
        Iterator<Snippet> snippetIterator = snippets.iterator();
        while (snippetIterator.hasNext()) {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Snippets");

            Row firstRow = sheet.createRow(0);
            int cellNumber = 0;
            for (String header : headers) {
                firstRow.createCell(cellNumber++).setCellValue(header);
            }

            int rownum = 1;
            while (snippetIterator.hasNext()) {
                Snippet snippet = snippetIterator.next();

                Row row = sheet.createRow(rownum++);
                int cellnum = 0;
                row.createCell(cellnum++).setCellValue(snippet.getQuery().getBody());
                row.createCell(cellnum++).setCellValue(snippet.getId());
                row.createCell(cellnum++).setCellValue(snippet.getSnippet());
                row.createCell(cellnum++).setCellValue(snippet.getPageUrl());
                row.createCell(cellnum++).setCellValue(snippet.getCachePageUrl());

                String pageBodyCellValue = snippet.getPageBody().length() > MAX_CELL_SIZE
                        ? snippet.getPageBody().substring(0, MAX_CELL_SIZE - 1)
                        : snippet.getPageBody();
                row.createCell(cellnum++).setCellValue(pageBodyCellValue);

                row.createCell(cellnum++).setCellValue(snippet.getDescription());
                row.createCell(cellnum).setCellValue(snippet.getSnippetBody());

                if (rownum == MAX_ROWS_COUNT) {
                    FileOutputStream out = new FileOutputStream(new File(path + "\\" + FILE_NAME_PATTERN + filePrefix + ".xls"));
                    workbook.write(out);
                    out.close();

                    rownum = 1;
                    filePrefix = String.valueOf(++fileNumber);

                    break;
                }
            }

            if (rownum > 1) {
                FileOutputStream out = new FileOutputStream(new File(path + "\\" + FILE_NAME_PATTERN + filePrefix + ".xls"));
                workbook.write(out);
                out.close();
            }
        }

    }
}
