package db;

/**
 * Created by Vladislav on 24.10.2015.
 */
public class DBException extends Exception {

    public DBException(String message, Exception clause) {
        super(message, clause);
    }
}
