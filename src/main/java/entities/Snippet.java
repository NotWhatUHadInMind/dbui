package entities;

import javax.persistence.*;

/**
 * Created by Vladislav on 11.10.2015.
 */

@Entity
@Table(name = "snippet")
public class Snippet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "queryId")
    private Query query;

    @Column
    private String snippet;

    @Column
    private String pageUrl;

    @Column
    private String cachePageUrl;

    @Column
    private String pageBody;

    @Column
    private String description;

    @Column
    private String snippetBody;

    @Column
    private Boolean checked;

    public Snippet() {
    }

    public Snippet(Query query, String snippet, String pageUrl, String cachePageUrl, String pageBody,
                   String description, String snippetBody, boolean checked) {
        this.query = query;
        this.snippet = snippet;
        this.pageUrl = pageUrl;
        this.cachePageUrl = cachePageUrl;
        this.pageBody = pageBody;
        this.description = description;
        this.snippetBody = snippetBody;
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getSnippetBody() {
        return snippetBody;
    }

    public void setSnippetBody(String snippetBody) {
        this.snippetBody = snippetBody;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getCachePageUrl() {
        return cachePageUrl;
    }

    public void setCachePageUrl(String cachePageUrl) {
        this.cachePageUrl = cachePageUrl;
    }

    public String getPageBody() {
        return pageBody;
    }

    public void setPageBody(String pageBody) {
        this.pageBody = pageBody;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "Snippet{" +
                "id=" + id +
                ", query=" + query +
                ", snippet='" + snippet + '\'' +
                ", pageUrl='" + pageUrl + '\'' +
                ", cachePageUrl='" + cachePageUrl + '\'' +
                ", pageBody='" + pageBody + '\'' +
                ", description='" + description + '\'' +
                ", snippetBody='" + snippetBody + '\'' +
                ", checked=" + checked +
                '}';
    }
}
